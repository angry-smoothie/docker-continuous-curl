# Continuous-curl

A simple container that executes curl commands.
It can repeat the curl command indefinitly with a custom interval.

The container is based on Alpine Linux 3.3 and uses Tini as PID 1.

## Usage :

```
docker run [-d|--rm] [OPTION...] continuous-curl URL
```

##### Available env variables:
   - DEBUG [0|1] Display the curl command before it executed
   - METHOD [PUT|GET|POST...] Add an optional HTTP Method
   - USER, PASSWORD Add credentials to the curl command
   - LOOP [seconds] Repeat the curl command every specified seconds
   - OPTIONS Any custom option that curl can understand



## Examples:

```
docker run --rm DEBUG=1 continuous-curl http://www.google.com
```
```
docker run -d -e LOOP=4 continuous-curl http://www.google.com
```
```
docker run -d -METHOD=PUT -e USER=john -e PASSWORD=shazam -e LOOP=4 continuous-curl http://www.mydomain.com/mywebservice
```

## Pull container on Docker Hub:
https://hub.docker.com/r/plavignotte/continuous-curl/