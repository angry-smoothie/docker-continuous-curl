#!/bin/sh

[ -z "$1" ] && echo "You must provide at least a URL" && exit 1

[ -n "$USER" ] && [ -n "$PASSWORD" ] && CURL_CREDENTIALS="-u '$USER:$PASSWORD'"
[ -n "$METHOD" ] && CURL_METHOD="-X $METHOD"

CURL_OPTIONS="$OPTIONS $CURL_METHOD $CURL_CREDENTIALS"

[ "$DEBUG" == "1" ]  && echo ">>>>> curl $CURL_OPTIONS $@"

if [ -n "$LOOP" ]
then
   watch -n $LOOP curl $CURL_OPTIONS $@
else
   curl $CURL_OPTIONS $@
fi
