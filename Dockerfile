FROM alpine:3.3

RUN apk add --update \
    --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ \
    tini \
    curl \
    && rm -rf /var/cache/apk/*

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/sbin/tini", "--", "/entrypoint.sh"]

